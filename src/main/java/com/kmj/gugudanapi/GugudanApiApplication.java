package com.kmj.gugudanapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GugudanApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GugudanApiApplication.class, args);
    }

}
