package com.kmj.gugudanapi;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GugudanRepository extends JpaRepository<Long,Gugudan> {
}
